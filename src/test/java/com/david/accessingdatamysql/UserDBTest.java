package com.david.accessingdatamysql;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class UserDBTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @BeforeEach
    void setUp() {
        JdbcTestUtils.deleteFromTables(jdbcTemplate, "users");
    }

    @Test
    void greetingShouldReturnNoRecords() throws Exception {
        mockMvc.perform(get("/app/all"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    void greetingShouldReturnOneRecords() throws Exception {
        mockMvc.perform(get("/app/all"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));

        mockMvc.perform(post("/app/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"name\": \"David1\"," +
                        " \"email\": \"david1.orelowitz@gmail.com\"," +
                        " \"address\":  {" +
                        "            \"street\": \"Lake View 221\"," +
                        "            \"city\": \"West Berlin\"," +
                        "            \"state\": \"Weat Berlin\"," +
                        "            \"zipCode\": \"95782\"," +
                        "            \"country\": \"DE\"" +
                        "   }" +
                        "}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mockMvc.perform(get("/app/all"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)));

        mockMvc.perform(post("/app/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"name\": \"David2\"," +
                        " \"email\": \"david2.orelowitz@gmail.com\"," +
                        " \"address\":  {" +
                        "            \"street\": \"Lake View 222\"," +
                        "            \"city\": \"West Berlin\"," +
                        "            \"state\": \"Weat Berlin\"," +
                        "            \"zipCode\": \"95782\"," +
                        "            \"country\": \"DE\"" +
                        "   }" +
                        "}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mockMvc.perform(get("/app/all"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));

        mockMvc.perform(get("/app/user/2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(2)))
                .andExpect(jsonPath("$.name", is("David2")));

        mockMvc.perform(delete("/app/user/2"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/app/user/2"))
                .andExpect(status().isNotFound());

        mockMvc.perform(get("/app/all"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)));
    }
}
