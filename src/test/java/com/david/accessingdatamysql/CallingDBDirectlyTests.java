package com.david.accessingdatamysql;

import com.david.accessingdatamysql.controller.MainController;
import com.david.accessingdatamysql.model.User;
import com.david.accessingdatamysql.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class CallingDBDirectlyTests {

	@Autowired
	private MainController controller;

	@Autowired
	private UserRepository userRepository;

	@BeforeEach
	void setUp() {
		userRepository.deleteAll();
	}


	// Check that Controller comes up
	@Test
	void contextLoads() {
		assertNotNull(controller);
	}

	@Test
	void writeTest() {

		userRepository.save(new User("David1", "david.orelowitz1@gmail.com"));
		userRepository.save(new User("David2", "david.orelowitz2@gmail.com"));

		Optional<User> foundEntity = userRepository.findById(1);
		assertTrue(foundEntity.isPresent() && foundEntity.get().getName().equals("David1"));
		foundEntity = userRepository.findById(2);
		assertTrue(foundEntity.isPresent() && foundEntity.get().getName().equals("David2"));
	}

	@Test
	void writeReadTest() {

		userRepository.save(new User("David3", "david.orelowitz3@gmail.com"));
		userRepository.save(new User("David4", "david.orelowitz4@gmail.com"));

		List<User> userList = userRepository.findByName("David4");
		assertEquals(1, userList.size());
		for (User retUser : userList) {
			assertEquals("David4", retUser.getName());
		}
	}

	@Test
	void createUserWithAddress() {
		User user = new User("John Doe", "john.doe@example.com"); //, new Address("Lake View 321", "Berlin", "Berlin", "95781", "DE"));
		userRepository.save(user);

		user = new User("David Doe", "david.doe@example.com"); //, new Address("Lake View 322", "Berlin", "Berlin", "95781", "DE"));
		userRepository.save(user);

		List<User> found = userRepository.findByName("John Doe");
		assertEquals(1, found.size());
		user  = found.get(0);
		assertEquals("John Doe", user.getName());
//		assertTrue(user.getAddress().getStreet().equals("Lake View 321"));
	}
}
