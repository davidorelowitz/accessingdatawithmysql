package com.david.accessingdatamysql.controller;

import com.david.accessingdatamysql.model.User;
import com.david.accessingdatamysql.repository.UserRepository;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController // This means that this class is a Controller
@RequestMapping(path="/app") // This means URL's start with /demo (after Application path)
public class MainController {
    @Autowired
    private UserRepository userRepository;

    @PostMapping(path="/user") // Map ONLY POST Requests
    public User addNewUser (@RequestBody User user) {
        userRepository.save(user);
        return user;
    }

    @DeleteMapping("/user/{id}")
    public ResponseEntity<User> deleteUser(@PathVariable(value = "id") Integer userId) throws ResourceNotFoundException {
        Optional<User> user = userRepository.findById(userId);
        if(user.isEmpty()) {
            return(ResponseEntity.notFound().build());
        } else {
            userRepository.delete(user.get());
            return(ResponseEntity.ok().build());
        }
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<User> getuser(@PathVariable(value = "id") Integer userId) {
        Optional<User> user = userRepository.findById(userId);
        if(user.isEmpty()) {
            return(ResponseEntity.notFound().build());
        } else {
            return(new ResponseEntity<>(user.get(), HttpStatus.OK));
        }
    }

    @GetMapping(path="/all")
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }
}